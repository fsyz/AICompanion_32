# AI伴侣32位  
32位桌面版AI伴侣  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0802/211557_6457a590_5567821.png "屏幕截图.png")  

#### 介绍
AI伴侣 32位
无需安装aiStarter，直接运行bat就能在电脑上启动AI伴侣，启动速度比虚拟机快很多。缺点是对硬件的要求比较高。  
文件来自 https://mp.weixin.qq.com/s/qb5RPrSuEd-OtAoPKpGDnQ 后的修改。 谢谢金老师的分享。  
App.gzjkw.net用不了看本文末尾。  
#### 使用说明

1.AI247_USB.bat  运行2.47版AI伴侣（app.gzjkw.net用）。或者  AI2Companion.bat 运行最新版AI伴侣, 当前是2.61版  
2.在网页设计窗口选择"连接"--"USB"连接AI伴侣。  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0731/214425_5f3c6687_5567821.jpeg "在这里输入图片标题")

#### 金从军老师 2021 MIT App Inventor个人版   
删除 AINEW_USB\vendor\chromium\crx目录下的apk程序, 下载https://gitee.com/fsyz/dnplayer  的MITAI2Companion2_6_0.apk到这目录下, 运行AI2Companion.bat,按使用说明操作.   

#### App.gzjkw.net 与最新版Chrome   
因为安全的原因，Chrome 新版本 94之后，不允许线上远端的站点请求 本地local 的地址，比如 ip 地址，会报 CORS 错误。    
临时解决办法：   
Chrome地址栏输入 chrome://flags/#block-insecure-private-network-requests  
修改Block insecure private network requests 设置值 Default 修改为 Disabled, 然后点击此页面下方的Relaunch 按钮，重新打开运行Chrome。   
参考资料： https://kebingzao.com/2021/10/11/chrome-private-cors-error/    