@echo off
%1(start /min cmd.exe /c %0 :&exit)
chcp 65001
echo "Starting aiStarter..."

title aiStarter
taskkill /F /T /IM nw.exe

for /f "tokens=1-5" %%i in ('netstat -ano^|findstr ":8004"') do (
    echo kill the process %%m  use the port 8004
    taskkill /F /T /pid %%m
)
cd /d %~dp0
 start  nw.exe --load-extension=ARChon --user-data-dir=%~dp0user_data --ignore-gpu-blacklist --enable-gcm  AI247_USB
