import os
filename = 'runnable-ld.so'
size = os.stat(filename).st_size
fh = open(filename, 'r+b')
fh.truncate((size + 0xffff) & ~0xffff)
fh.close()
exit()
